const express = require('express')
const mongoose = require('mongoose');
const app = express()
const booksRoutes = require('./routes/booksRoutes')
const authorRoutes = require('./routes/authorRoutes')
const userRoutes = require('./routes/userRoutes')
const cors = require('cors');
require('dotenv').config()

const PORT = 3000

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());


app.use('/books', booksRoutes)
app.use('/authors',authorRoutes )
app.use('/users',userRoutes )



app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});


main().then(()=>console.log("Connected")).catch(err => console.log(err));

async function main() {
  await mongoose.connect("mongodb+srv://sarathkalarikkal0:xsYJco6gtRYleiBX@cluster0.8lgnzwx.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0");
}
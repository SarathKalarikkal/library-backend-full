const Books = require("../model/bookModel");

//------------------------Get all the books from DB------------------------
const getAllBooks = async(req, res)=> {
    const books = await Books.find({})
     res.json(books)
  }



  //------------------------Get a single book by id from DB------------------------
const getSingleBook =async(req, res)=> {
  const book =  await Books.findById(req.params.id);
  if (book) {
    res.status(404).json(book)
  } else {
    res.status(404).send('Book not found');
  }
}



//------------------------Add a books to DB------------------------
  const addBook = async (req, res) => {
    try {
      const { bookName, image, publishedAt,author, description, price } = req.body;
      const book = await Books.create({
        bookName: bookName, 
        image: image,
        publishedAt :publishedAt,
        author : author,
        description: description,
        price : price
      });
      res.json(book);
    } catch (error) {
      res.status(500).json({ message: "Failed to add book", error: error });
    }
  }



  //------------------------Update a book by id from DB------------------------
  const updateAbook = async (req, res) => {
      try {
          const book = await Books.findById(req.params.id);
          if (book) {
              const {bookName, image, publishedAt,author, description, price } = req.body;
              await book.updateOne({
                bookName: bookName, 
                image: image,
                publishedAt :publishedAt,
                author : author,
                description : description,
                price : price
              })
              res.json(book)
          } else {
              res.json({ message: "Book not found" })
          }
      } catch (error) {
          res.status(500).json({ message: "Failed to update book", error: error });
      }
  }


  //------------------------Delete a book by id from DB------------------------
  const deleteBook =async(req, res)=> {
     const book =  await Books.findById(req.params.id);
     if (book) {
       await book.deleteOne();
       res.send('Book deleted successfully');
     } else {
       res.status(404).send('Book not found');
     }
  }


module.exports = {getAllBooks, getSingleBook, addBook, updateAbook,deleteBook}